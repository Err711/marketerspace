# Generated by Django 4.1 on 2022-09-01 17:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("Users", "0003_alter_myuser_email"),
        ("Organizations", "0002_alter_organization_name"),
    ]

    operations = [
        migrations.RenameModel(old_name="Organization", new_name="OrganizationModel",),
    ]
