from django.contrib import admin

from .models import CustomOrganization

admin.site.register(CustomOrganization)
