from rest_framework import serializers
from Organizations.models import CustomOrganization


class OrganizationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomOrganization
        fields = "__all__"


class OrganizationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomOrganization
        fields = "__all__"
