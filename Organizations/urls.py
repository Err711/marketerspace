from django.contrib import admin
from django.urls import path, include
from Organizations.views import *


app_name = "Organizations"

urlpatterns = [
    path("organization/create/", OrganizationCreateView.as_view()),
    path("organization/update/<int:pk>/", OrganizationUpdateView.as_view()),
    path("organization/delete/<int:pk>/", OrganizationDestroyView.as_view()),
    path("organization/list/", OrganizationListView.as_view()),
]

