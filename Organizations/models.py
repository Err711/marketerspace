from django.db import models


class CustomOrganization(models.Model):
    name = models.CharField(max_length=200, verbose_name="Organization name", unique=True)
    domain = models.TextField(unique=True)

    def __str__(self):
        return self.name
