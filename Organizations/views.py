from django.shortcuts import render
from rest_framework import generics

import Organizations
from Organizations.models import CustomOrganization
from Organizations.permissions import IsAdminOrReadOnly, IsOwnerOrReadOnly
from Organizations.serializers import OrganizationCreateSerializer, OrganizationListSerializer


class OrganizationCreateView(generics.CreateAPIView):
    serializer_class = OrganizationCreateSerializer
    permission_classes = (IsAdminOrReadOnly, )


class OrganizationUpdateView(generics.RetrieveUpdateAPIView):
    serializer_class = OrganizationCreateSerializer
    permission_classes = (IsAdminOrReadOnly, )


class OrganizationDestroyView(generics.RetrieveDestroyAPIView):
    serializer_class = OrganizationCreateSerializer
    permission_classes = (IsAdminOrReadOnly, )


class OrganizationListView(generics.ListAPIView):
    serializer_class = OrganizationListSerializer
    queryset = CustomOrganization.objects.all()
