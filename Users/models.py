from django.db import models
from django.contrib.auth.models import AbstractUser

from Organizations.models import CustomOrganization


def profile_image_path(instance, filename):
    print(instance, filename)
    return f"profile_images/{instance.user.username}/{filename}"


class CustomUser(AbstractUser):
    profile_picture = models.ImageField(upload_to=profile_image_path, default='static/images/profile_pic.png')
    organization = models.ForeignKey(CustomOrganization, on_delete=models.CASCADE, null=True, blank=True)
    country = models.TextField(max_length=100)
    creation_date = models.DateField(auto_now_add=True)
    modification_date = models.DateField(auto_now=True)


    # USERNAME_FIELD = 'email'


