from django.contrib import admin
from django.urls import path, include
from Users.views import *


app_name = "User"

urlpatterns = [
    path("user/create/", UserCreateView.as_view()),
    path("user/list/", UserListView.as_view()),
]
