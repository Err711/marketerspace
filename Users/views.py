from django.shortcuts import render
from rest_framework import generics
from Users.serializers import UserCreateSerializer, UserListSerializer
from Users.models import CustomUser


class UserCreateView(generics.CreateAPIView):
    serializer_class = UserCreateSerializer


class UserListView(generics.ListAPIView):
    serializer_class = UserListSerializer
    queryset = CustomUser.objects.all()
